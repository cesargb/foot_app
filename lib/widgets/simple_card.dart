
import 'package:flutter/material.dart';

class SimpleCard extends StatelessWidget {
  final Widget header;
  final Widget body;
  final Function onTap;
  final double borderRadius;
  final double bodyPadding;
  const SimpleCard({
    Key key,
    @required this.header,
    @required this.body,
    this.onTap,
    this.borderRadius = 4,
    this.bodyPadding=4,
  }) : super(key: key);

  @override
    Widget build(BuildContext context) {
      return Card(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(borderRadius),
          child: Stack(
            children: ((){
              List<Widget> l = [
                Column(
                  children: <Widget>[
                    header,
                    Padding(
                      padding: EdgeInsets.all(bodyPadding),
                      child: body,
                    ),
                  ],
                )
              ];
              if(onTap != null){
                l.add(
                    Positioned.fill(
                        child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(80),
                              onTap: onTap,
                            )
                        )
                    )
                );
              }
              return l;
            })(),
        ),
      ),
    );
  }
}


