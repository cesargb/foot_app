import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:foot_app/models/business.dart';
import 'package:foot_app/widgets/simple_card.dart';


class RestCard extends StatefulWidget {
  final GestureTapCallback onTap;
  final Business model;
  RestCard({Key key, this.onTap, @required this.model,}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<RestCard> {
  @override
  Widget build(BuildContext context)
    =>  SimpleCard(
        header: Image(
            image: NetworkImageWithRetry( widget.model.media_url),
            width: double.infinity,
            height: 200,
            fit: BoxFit.cover,
        ),
        body: Text(widget.model.name,style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
      onTap: ()=> widget.onTap(),
    );
}




