import 'dart:math';

import 'package:flutter/material.dart';

class PageHeader extends StatelessWidget{
  final Image background;
  final Image brand;
  final GestureTapCallback onTab;
  PageHeader({@required this.background, this.brand,this.onTab});





  @override
  Widget build(BuildContext ctx)
    => SliverAppBar(
      pinned: false,
      expandedHeight: 250.0,
      flexibleSpace: Directionality(
        textDirection: TextDirection.rtl,
        child: GestureDetector(
          onTap: onTab,
          child: FlexibleSpaceBar(
            background: this.background, //Image.asset("assets/img/th_p2.jpg",fit: BoxFit.cover),

            title: SizedBox(
              height: 60,
              width: 60,
              child:this.brand,//  Image.asset("assets/img/th.png"),
            ),
            //child: Container(color: Colors.black87)),
            titlePadding: EdgeInsets.fromLTRB(0,0,8,0),
            centerTitle: false,
          ),
        ),
      ),

    );
}