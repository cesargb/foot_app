

import 'package:flutter/widgets.dart';

class AppProvider extends ChangeNotifier{
  String _title = "Food App";
  String get title => _title;
  set title(String value){

    _title = value;
    notifyListeners();
  }
}