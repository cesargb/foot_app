

import 'package:flutter/foundation.dart';

class Orderinfo with ChangeNotifier {
  List<OrderItem> _orderList = [];

  List<OrderItem> orderList() => _orderList;


  addItem(OrderItem item){
      _orderList.add(item);
      notifyListeners();
  }

  delItem(int index){
    if(index >=0 && index < _orderList.length) {
      _orderList.removeAt(index);
      notifyListeners();
    }
  }
}

class OrderItem{
  Foot footItem;
  int count;
}

class Foot{
  int foodId;
  String name;

}