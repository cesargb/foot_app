
import 'package:foot_app/models/business.dart';
import 'package:foot_app/models/dish.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:foot_app/core/reply.dart';
import 'package:dson/dson.dart';

class  SearchFilter{
  final int id;
  final int from_id;
  final int to_id;
  final int max_count;
  final int from_date;
  final int to_date;
  final bool revert;

  const SearchFilter({this.id, this.from_id, this.to_id, this.max_count,
      this.from_date, this.to_date, this.revert});

}


class BusinessService{ //jsonSer
  final String list_url = "http://192.168.1.72:8080/business";
  final String dishes_url = "http://192.168.1.72:8080/dishes";

  Future<Reply<List<Business>>> list({SearchFilter filter = const SearchFilter()}) async {
    var r = await http.get(list_url);
    //if(r.statusCode != 200) return Reply.error("Error al realizar peticion",code:r.statusCode);

   // var json = convert.jsonDecode(r.body);
    var reply = fromJson(r.body,BussinessReply);
    return reply;

  }
  // ignore: missing_return
  Future<Reply<List<Dish>>> dishes(int id) async {

//    var url = Uri.http("10.11.2.74:51334","ItemPresentation/ActionDishes",{
//      "id":id.toString(),
//    });
//
//    var r = await http.get(url);
//   // if(r.statusCode != 200) return Reply.error("Error al realizar peticion",code:r.statusCode);
//
//    var json = convert.jsonDecode(r.body);
//    Reply<List<Dish>> reply = Reply(
//      code: json['Code'],
//      msg: json['Msg'],
//      ok :json['Ok'],
//
//    );
//    var datalist = json['Data'] as List;
//    reply.data = datalist.map((d) {
//      return  Dish(
//        dish_id: d['dish_id'],
//        name: d['name'],
//        active: d['active'],
//        price: d['price'].toDouble(),
//        limit_num: d['limit_num'],
//        unit: d['unit'],
//        media_id: d['media_id'],
//        media_url:d['media_url'],
//      );
//    }).toList();
//    return reply;
  }
}

//class BusinessServiceM{ //jsonManual
//  final String list_url = "http://10.11.2.74:51334/ItemPresentation/ActionBusiness";
//  final String dishes_url = "http://10.11.2.74:51334/ItemPresentation/ActionDishes";
//
//  Future<Reply<List<Business>>> list({SearchFilter filter = const SearchFilter()}) async {
//    var r = await http.get(list_url);
//    if(r.statusCode != 200) return Reply.error("Error al realizar peticion",code:r.statusCode);
//
//    var json = convert.jsonDecode(r.body);
//    Reply<List<Business>> reply = Reply(
//      code: json['Code'],
//      msg: json['Msg'],
//      ok :json['Ok'],
//
//    );
//
//    var datalist = json['Data'] as List;
//    reply.data = datalist.map((d) {
//      return Business(
//          business_id: d['business_id'],
//          name :d['name'],
//          description: d['description'],
//          active: d['active'],
//          media_id: d['media_id'],
//          media_url: d['media_url'],
//
//        );
//    }).toList();
//    return reply;
//
//  }
//  Future<Reply<List<Dish>>> dishes(int id) async {
//    var url = Uri.http("10.11.2.74:51334","ItemPresentation/ActionDishes",{
//      "id":id.toString(),
//    });
//
//    var r = await http.get(url);
//    if(r.statusCode != 200) return Reply.error("Error al realizar peticion",code:r.statusCode);
//
//    var json = convert.jsonDecode(r.body);
//    Reply<List<Dish>> reply = Reply(
//      code: json['Code'],
//      msg: json['Msg'],
//      ok :json['Ok'],
//
//    );
//    var datalist = json['Data'] as List;
//    reply.data = datalist.map((d) {
//      return  Dish(
//          dish_id: d['dish_id'],
//          name: d['name'],
//          active: d['active'],
//          price: d['price'].toDouble(),
//          limit_num: d['limit_num'],
//          unit: d['unit'],
//          media_id: d['media_id'],
//          media_url:d['media_url'],
//      );
//    }).toList();
//    return reply;
//  }
//}
//
//
//
//
//
//class BusinessService{
//  static const  Map<int,List<Dish>> DISHES = const{
//    1 : [
//      Dish(dish_id: 1,name: "Taco",active: true,price: 25.0,limit_num: 20,unit: "PZA",media_id: 10,media_url:"https://scontent.ftij3-1.fna.fbcdn.net/v/t1.0-9/67401934_317478252467856_145967169383956480_n.jpg?_nc_cat=101&_nc_oc=AQknOBc6cmLiMUeTTa_VlA9CMBJnxOZdWaNCwUkMGfZgky1rWIB2gbDDHUjjKvNL9Ps&_nc_ht=scontent.ftij3-1.fna&oh=c2654bedc387c25327960c08c3b3d7c4&oe=5DE1739E"),
//      Dish(dish_id: 1,name: "Chicarron ribeye",active: true,price: 180,limit_num: 5,unit: "PZA",media_id: 10,media_url:"https://scontent.ftij3-1.fna.fbcdn.net/v/t1.0-9/64530137_319148698967478_658116769319223296_o.jpg?_nc_cat=105&_nc_oc=AQmEJeFm6pnmpmwOfdjVdTcH1_nf06oQRekGLJeP5Lw_Xg3Z4Y0ZxVVhS2Jc3nw-K6A&_nc_ht=scontent.ftij3-1.fna&oh=f2beab8f05abd97cbfd5d7fe441a8520&oe=5DC96FAA"),
//      Dish(dish_id: 1,name: "Chorreada",active: true,price: 35,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://scontent.ftij3-1.fna.fbcdn.net/v/t1.0-9/67837126_332684180947263_7320214874867367936_o.jpg?_nc_cat=110&_nc_oc=AQlXnUonAEVHRkOT8tWGczrcyjcuu2CYlCTDA-5njfOeL7PAIHl4dIL7za8f9TrMP8s&_nc_ht=scontent.ftij3-1.fna&oh=a28ff4a7d60536b8d187e3fb6c2bca53&oe=5E149CB1"),
//
//    ],
//    2:[
//      Dish(dish_id: 1,name: "Hamburguesa",active: true,price: 55.0,limit_num: 20,unit: "PZA",media_id: 10,media_url:"https://www.hogar.mapfre.es/wp-content/uploads/2018/09/hamburguesa-sencilla.jpg"),
//      Dish(dish_id: 1,name: "Papas",active: true,price: 35.0,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://e.rpp-noticias.io/normal/2017/01/25/544154_333355.png"),
//      Dish(dish_id: 1,name: "Combo",active: true,price: 80.0,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://www.fmdos.cl/wp-content/uploads/2017/11/hamburguesa-con-papas-fritas-1024x577.jpg"),
//    ],
//    3:[
//      Dish(dish_id: 1,name: "Pizza peperoni",active: true,price: 79,limit_num: 20,unit: "PZA",media_id: 10,media_url:"https://placeralplato.com/files/2016/01/Pizza-con-pepperoni.jpg"),
//      Dish(dish_id: 1,name: "Pizza 3 carnes",active: true,price: 129,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://www.cicis.com/wp-content/uploads/2019/02/es_pizza_trad_pepperonibeef_sm.png"),
//      Dish(dish_id: 1,name: "Pizza super queso",active: true,price: 150,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://cdn-3.expansion.mx/dims4/default/f412254/2147483647/strip/true/crop/3000x2255+0+0/resize/800x601!/quality/90/?url=https%3A%2F%2Fcdn-3.expansion.mx%2F72%2F73%2Fdaf452fb4236a6d6745068300984%2Fs097056563.JPG"),
//    ],
//    4:[
//      Dish(dish_id: 1,name: "Sushi",active: true,price: 79,limit_num: 20,unit: "PZA",media_id: 10,media_url:"https://images.japancentre.com/recipes/pics/18/main/makisushi.jpg?1557308201"),
//      Dish(dish_id: 1,name: "teriyaki",active: true,price: 129,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://cdn.sallysbakingaddiction.com/wp-content/uploads/2017/09/slow-cooker-honey-teriyaki-chicken-2.jpg"),
//      Dish(dish_id: 1,name: "Fideos",active: true,price: 150,limit_num: 10,unit: "PZA",media_id: 10,media_url:"https://www.mercadoflotante.com/blog/wp-content/uploads/2016/05/ramen-1.jpg"),
//    ]
//  };
//  Future<Reply<List<Business>>> list() async {
//    return Future.delayed(Duration(seconds: 0), ()=> Reply.ok(
//      [
//        Business(
//          business_id: 1,
//          name:"Takito house",
//          description: "Un lugar de tacos",
//          active: true,
//          media_id: 1,
//          media_url:"https://scontent.ftij3-1.fna.fbcdn.net/v/t1.0-9/67775418_332684144280600_359305131348459520_n.jpg?_nc_cat=100&_nc_oc=AQmAbxlXcwcmUs9Kv3wFFXh_o80-HOd5CwPG9OrB-zYzdLwOjqf4oZnu2jufG44ApdQ&_nc_ht=scontent.ftij3-1.fna&oh=6b409ff4c0100074ee37872789413323&oe=5DC96921",
//
//        ),Business(
//          business_id: 2,
//          name:"Beberly burguers",
//          description: "Un lugar de hamburguesas",
//          active: true,
//          media_id: 2,
//          media_url:"https://pbs.twimg.com/media/Cf3Lb3OVAAAXJ1f.jpg",
//
//
//        ),Business(
//          business_id: 3,
//          name:"Little Cessars",
//          description: "Un lugar de pizza",
//          active: true,
//          media_id: 3,
//          media_url:"http://mty360.net/wp-content/uploads/2018/01/Little-Caesars-se-consolida-con-su-nueva-pizza-Super-Cheese%C2%AE.jpg",
//
//
//        ),Business(
//          business_id: 4,
//          name:"La postal",
//          description: "Un lugar de sushi",
//          active: true,
//          media_id: 4,
//          media_url:"https://img.juridipedia.com/1/v/t1.0-9/s720x720/66336090_2086179538351513_8046815447477125120_n.jpg?_nc_cat=102&_nc_oc=AQlymBRKIomMAji06Q2TC8sC_ogmdt1jhd7zZvIbTdYLP1GmZwNIOOpVzXxNAiG-hQGxUuEnK_zruwO-mq_-3x3p&_nc_ht=scontent.xx&oh=b21401c93ac21830ed972fd3de4ba286&oe=5DBD3FB9",
//
//        ),
//      ])
//    );
//
//  }
//  Future<Reply<List<Dish>>> dishes(int id) async {
//    return Reply.ok(DISHES[id]);
//  }
//
//
//
//}