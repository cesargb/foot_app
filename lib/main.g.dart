// GENERATED CODE - DO NOT MODIFY BY HAND

part of example.main;

// **************************************************************************
// InitMirrorsGenerator
// **************************************************************************

_initMirrors() {
  initClassMirrors({
    Reply: ReplyClassMirror,
    Business: BusinessClassMirror,
    BussinessReply: BussinessReplyClassMirror
  });

  getClassMirrorFromGenericInstance =
      (instance) => instance is Reply ? ReplyClassMirror : null;
}
