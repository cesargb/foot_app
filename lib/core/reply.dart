library example.reply;
import 'package:dson/dson.dart';
part 'reply.g.dart';

@serializable
class Reply<T> extends _$ReplySerializable<T>{

   String msg;
   T data;
   int code;
   bool ok;
   Reply();

//   static Reply<T> OK<T>(T data) {
//     var
//    return  Reply(data: data,msg: null,code: 0,ok: true);
//   }
//
//  static Reply<T> error<T>(String msg,{int code=3}){
//    return  Reply(data: null,msg: msg,code: code ,ok: false);
//  }

}
