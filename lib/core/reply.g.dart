// GENERATED CODE - DO NOT MODIFY BY HAND

part of example.reply;

// **************************************************************************
// DsonGenerator
// **************************************************************************

abstract class _$ReplySerializable<T> extends SerializableMap {
  String get msg;
  T get data;
  int get code;
  bool get ok;
  set msg(String v);
  set data(T v);
  set code(int v);
  set ok(bool v);

  operator [](Object __key) {
    switch (__key) {
      case 'msg':
        return msg;
      case 'data':
        return data;
      case 'code':
        return code;
      case 'ok':
        return ok;
    }
    throwFieldNotFoundException(__key, 'Reply');
  }

  operator []=(Object __key, __value) {
    switch (__key) {
      case 'msg':
        msg = __value;
        return;
      case 'data':
        data = __value;
        return;
      case 'code':
        code = __value;
        return;
      case 'ok':
        ok = __value;
        return;
    }
    throwFieldNotFoundException(__key, 'Reply');
  }

  Iterable<String> get keys => ReplyClassMirror.fields.keys;
}

// **************************************************************************
// MirrorsGenerator
// **************************************************************************

_Reply__Constructor([positionalParams, namedParams]) => new Reply();

const $$Reply_fields_msg = const DeclarationMirror(name: 'msg', type: String);
const $$Reply_fields_data =
    const DeclarationMirror(name: 'data', type: dynamic);
const $$Reply_fields_code = const DeclarationMirror(name: 'code', type: int);
const $$Reply_fields_ok = const DeclarationMirror(name: 'ok', type: bool);

const ReplyClassMirror = const ClassMirror(name: 'Reply', constructors: const {
  '': const FunctionMirror(name: '', $call: _Reply__Constructor)
}, fields: const {
  'msg': $$Reply_fields_msg,
  'data': $$Reply_fields_data,
  'code': $$Reply_fields_code,
  'ok': $$Reply_fields_ok
}, getters: const [
  'msg',
  'data',
  'code',
  'ok'
], setters: const [
  'msg',
  'data',
  'code',
  'ok'
]);
