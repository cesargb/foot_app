library example.main;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:foot_app/providers/app_provider.dart';
import 'package:foot_app/pages/home_page.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'services/business_service.dart';

import 'package:foot_app/core/reply.dart';
import 'package:foot_app/models/business.dart';


import 'package:dson/dson.dart';
part 'main.g.dart';
//jaguar_serializer
//fluro
//async_loader
const key = "AIzaSyA3BTzW8GZzRt9f_aGVkWaNe04l5CK2hmA";
void main() {
  _initMirrors();
  runApp(MyApp());

}
const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _blackPrimaryValue = 0xFF000000;
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
       Provider.value(value: BusinessService()),
       ChangeNotifierProvider.value( value:  AppProvider()),
      ],
      child: MaterialApp(
        title: 'Foot app',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: primaryBlack,
        ),
        home: HomePage(),
      ),
    );
  }
}

