import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:foot_app/providers/app_provider.dart';

import 'package:foot_app/models/business.dart';
import 'package:foot_app/models/dish.dart';
import 'package:foot_app/services/business_service.dart';
import 'package:foot_app/widgets/page_header.dart';
import 'package:foot_app/widgets/simple_card.dart';
import 'package:provider/provider.dart';



import 'package:async_loader/async_loader.dart';

class DishesPage extends StatelessWidget {
  final Business business;
  DishesPage({@required this.business});

  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();
  @override
  Widget build(BuildContext context) {
    final bussines_service = Provider.of<BusinessService>(context);
    final app = Provider.of<AppProvider>(context);

    var loader = AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await bussines_service.dishes(business.business_id),
      renderLoad: () =>  CircularProgressIndicator(),
      renderError: ([error]) =>  Text("Error al cargar datos"),
      renderSuccess:
          ({data}) =>
          Scaffold(
            appBar: AppBar(title: Text(business.name)),
            body: CustomScrollView(
              slivers: <Widget>[
                PageHeader(
                  background:  Image(
                      image: NetworkImageWithRetry( business.media_url),
                      width: double.infinity,
                      height: 200,
                      fit: BoxFit.cover),

                  onTab: ()=> {},
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate((ctx,i) => FoodCard(model:FoodModel(data.data[i])) ,childCount:data.data.length ),
                ),

              ],
            ),
          )
    );
    return loader;
  }

}

class FoodCard extends StatefulWidget {
  final FoodModel model;
  const FoodCard({@required this.model});

  @override
  _FoodCardState createState() => _FoodCardState();
}

class _FoodCardState extends State<FoodCard> {
  int counter=0;
  @override
  Widget build(BuildContext context) {
    
    return SimpleCard(

      header:Image(image: NetworkImageWithRetry( widget.model.data.media_url),
          width: double.infinity,
          height: 200,
          fit: BoxFit.cover) ,
      body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(widget.model.data.name,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),

                Text("\$${widget.model.data.price} ${widget.model.data.unit}")
              ],
            ),
            RaisedButton(child: Text("-"),onPressed: (){
              print("sasdfasdfasdfa");
              if(counter<=0) return;
              setState(() {
                counter -=1;
              });
            }),
            Text(counter.toString()),
            RaisedButton(child: Text("+"),onPressed: (){
              if(counter>=widget.model.data.limit_num) return;
              setState(() {
                counter +=1;
              });

            })
          ]),
    );

  }
}

class FoodModel {
  Dish data;
  int counter = 0;
  FoodModel(this.data);

}