import 'package:flutter/material.dart';

import 'map_page.dart';
import 'order_list_page.dart';
import 'rest_list_page.dart';
class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<HomePage> {
  int _currentIndex = 0;
  final _views = [
  {MapPage(),"A donde te llevamos tu comida?"},{RestListPage(),"¿Donde quieres comer hoy?"},{OrdersListPage(),"Food app"},
  ];

  @override
  Widget build(BuildContext context)
  => Scaffold(
    appBar: AppBar(title: Text(_views[_currentIndex].elementAt(1) )),
    body: _views[_currentIndex].elementAt(0),
    bottomNavigationBar: BottomNavigationBar(
        onTap: _onTab,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.map),
            title: new Text('Ubicación'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.cake),
            title: new Text('Menu'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.timer),
            title: new Text('Pedidos'),
          ),
        ]
    ),
  );
  void _onTab(int index){
    setState(() {
      _currentIndex = index;
    });
  }

}

