import 'package:flutter/material.dart';
import 'package:foot_app/providers/app_provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

import 'package:provider/provider.dart';



class MapPage extends StatefulWidget{


  @override
  State createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng( 32.522499,-117.046623),
    zoom: 14.4746,
  );
  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);


  @override
  Widget build(BuildContext context)
  {
    final app = Provider.of<AppProvider>(context);
    app.title = "¿A donde te llevamos tu comida?";
    return GoogleMap(
    mapType: MapType.normal,
    initialCameraPosition: _kGooglePlex,
    onMapCreated: (GoogleMapController controller) {
      _controller.complete(controller);
    },
  );
  }
}