


import 'dart:math';

import 'package:flutter/material.dart';
import 'package:foot_app/services/business_service.dart';
import 'package:foot_app/pages/dishes_page.dart';
import 'package:foot_app/widgets/page_header.dart';
import 'package:provider/provider.dart';


import 'package:foot_app/providers/app_provider.dart';

import 'package:foot_app/widgets/rest_card.dart';
import 'package:async_loader/async_loader.dart';



class RestListPage extends StatelessWidget{
  final GlobalKey<AsyncLoaderState> _asyncLoaderState =
  new GlobalKey<AsyncLoaderState>();

  @override
  Widget build(BuildContext context) {
    final bussines_service = Provider.of<BusinessService>(context);
    final app = Provider.of<AppProvider>(context);

    var loader = AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await bussines_service.list(),
      renderLoad: () =>  CircularProgressIndicator(),
      renderError: ([error]) =>  Text("Error al cargar datos"),
      renderSuccess:
          ({data}) =>
            CustomScrollView(
                  slivers: <Widget>[
                            PageHeader(
                              background:  Image.asset("assets/img/th_p2.jpg",fit: BoxFit.cover),
                              brand: Image.asset("assets/img/th.png"),
                              onTab: ()=>  print("header tap"),
                            ),
                            SliverList(
                              delegate: SliverChildBuilderDelegate((ctx,i) =>
                                  RestCard(
                                    model: data.data[i],
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (c)=>DishesPage(business: data.data[i])));
                                    },
                                  ),childCount:data.data.length ),
                            ),

                  ],
            ),
    );
    return loader;
  }


  
}






//class FoodModel{
//  int id;
//  String name;
//  String url;
//  int counter = 0;
//  FoodModel(this.id,this.name,this.url);
//
//}
//@override
//Widget build(BuildContext context) => asdf(context);
// final bussines_service = Provider.of<BusinessService>(context);

//Widget asdf(context) =>
//    CustomScrollView(
//      slivers: <Widget>[
//        PageHeader(
//          background:Image.asset("assets/img/th_p2.jpg",fit: BoxFit.cover),
//          brand: Image.asset("assets/img/th.png"),
//          onTab: ()=>  print("header tap"),
//        ),
//        SliverList(
//          delegate: SliverChildBuilderDelegate((ctx,i) => RestCard(model: restlist[i],),childCount:restlist.length ),
//        ),
//        SliverList(
//          delegate: SliverChildBuilderDelegate((ctx,i) => _buildRow(foodlist[i], i,ctx),childCount:foodlist.length ),
//        ),
//      ],
//    );

