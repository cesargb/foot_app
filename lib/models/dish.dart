class Dish {

  final int dish_id;
  final String name;
  final bool active;
  final int media_id;
  final int limit_num;
  final double price;
  final String unit;
  final String media_url;
  const Dish({this.dish_id, this.name, this.active, this.media_id, this.limit_num,
      this.price, this.unit, String this.media_url});

}