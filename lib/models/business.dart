library example.business;
import 'package:foot_app/core/reply.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:dson/dson.dart';
part 'business.g.dart';


@serializable
class Business extends _$BusinessSerializable {
   int business_id;
   String name;
   String description;
   bool active;
   int media_id;
   DateTime created_at;
   DateTime updated_at;
   String media_url;
   Business({this.business_id, this.name, this.description, this.active,
      this.media_id, this.created_at, this.updated_at, this.media_url});


}

class BusinessCategory{

}
@serializable
class BussinessReply extends Reply<List<Business>>  with _$BussinessReplySerializable {
   BussinessReply();
}