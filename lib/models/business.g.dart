// GENERATED CODE - DO NOT MODIFY BY HAND

part of example.business;

// **************************************************************************
// DsonGenerator
// **************************************************************************

abstract class _$BusinessSerializable extends SerializableMap {
  int get business_id;
  String get name;
  String get description;
  bool get active;
  int get media_id;
  DateTime get created_at;
  DateTime get updated_at;
  String get media_url;
  set business_id(int v);
  set name(String v);
  set description(String v);
  set active(bool v);
  set media_id(int v);
  set created_at(DateTime v);
  set updated_at(DateTime v);
  set media_url(String v);

  operator [](Object __key) {
    switch (__key) {
      case 'business_id':
        return business_id;
      case 'name':
        return name;
      case 'description':
        return description;
      case 'active':
        return active;
      case 'media_id':
        return media_id;
      case 'created_at':
        return created_at;
      case 'updated_at':
        return updated_at;
      case 'media_url':
        return media_url;
    }
    throwFieldNotFoundException(__key, 'Business');
  }

  operator []=(Object __key, __value) {
    switch (__key) {
      case 'business_id':
        business_id = __value;
        return;
      case 'name':
        name = __value;
        return;
      case 'description':
        description = __value;
        return;
      case 'active':
        active = __value;
        return;
      case 'media_id':
        media_id = __value;
        return;
      case 'created_at':
        created_at = fromSerializedDateTime(__value);
        return;
      case 'updated_at':
        updated_at = fromSerializedDateTime(__value);
        return;
      case 'media_url':
        media_url = __value;
        return;
    }
    throwFieldNotFoundException(__key, 'Business');
  }

  Iterable<String> get keys => BusinessClassMirror.fields.keys;
}

abstract class _$BussinessReplySerializable extends SerializableMap {
  String get msg;
  List<Business> get data;
  int get code;
  bool get ok;
  set msg(String v);
  set data(List<Business> v);
  set code(int v);
  set ok(bool v);

  operator [](Object __key) {
    switch (__key) {
      case 'msg':
        return msg;
      case 'data':
        return data;
      case 'code':
        return code;
      case 'ok':
        return ok;
    }
    throwFieldNotFoundException(__key, 'BussinessReply');
  }

  operator []=(Object __key, __value) {
    switch (__key) {
      case 'msg':
        msg = __value;
        return;
      case 'data':
        data = fromSerialized(
            __value, [() => new List<Business>(), () => new Business()]);
        return;
      case 'code':
        code = __value;
        return;
      case 'ok':
        ok = __value;
        return;
    }
    throwFieldNotFoundException(__key, 'BussinessReply');
  }

  Iterable<String> get keys => BussinessReplyClassMirror.fields.keys;
}

// **************************************************************************
// MirrorsGenerator
// **************************************************************************

_Business__Constructor([positionalParams, namedParams]) => new Business(
    business_id: namedParams['business_id'],
    name: namedParams['name'],
    description: namedParams['description'],
    active: namedParams['active'],
    media_id: namedParams['media_id'],
    created_at: namedParams['created_at'],
    updated_at: namedParams['updated_at'],
    media_url: namedParams['media_url']);

const $$Business_fields_business_id =
    const DeclarationMirror(name: 'business_id', type: int);
const $$Business_fields_name =
    const DeclarationMirror(name: 'name', type: String);
const $$Business_fields_description =
    const DeclarationMirror(name: 'description', type: String);
const $$Business_fields_active =
    const DeclarationMirror(name: 'active', type: bool);
const $$Business_fields_media_id =
    const DeclarationMirror(name: 'media_id', type: int);
const $$Business_fields_created_at =
    const DeclarationMirror(name: 'created_at', type: DateTime);
const $$Business_fields_updated_at =
    const DeclarationMirror(name: 'updated_at', type: DateTime);
const $$Business_fields_media_url =
    const DeclarationMirror(name: 'media_url', type: String);

const BusinessClassMirror =
    const ClassMirror(name: 'Business', constructors: const {
  '': const FunctionMirror(
      name: '',
      namedParameters: const {
        'business_id': const DeclarationMirror(
            name: 'business_id', type: int, isNamed: true),
        'name':
            const DeclarationMirror(name: 'name', type: String, isNamed: true),
        'description': const DeclarationMirror(
            name: 'description', type: String, isNamed: true),
        'active':
            const DeclarationMirror(name: 'active', type: bool, isNamed: true),
        'media_id':
            const DeclarationMirror(name: 'media_id', type: int, isNamed: true),
        'created_at': const DeclarationMirror(
            name: 'created_at', type: DateTime, isNamed: true),
        'updated_at': const DeclarationMirror(
            name: 'updated_at', type: DateTime, isNamed: true),
        'media_url': const DeclarationMirror(
            name: 'media_url', type: String, isNamed: true)
      },
      $call: _Business__Constructor)
}, fields: const {
  'business_id': $$Business_fields_business_id,
  'name': $$Business_fields_name,
  'description': $$Business_fields_description,
  'active': $$Business_fields_active,
  'media_id': $$Business_fields_media_id,
  'created_at': $$Business_fields_created_at,
  'updated_at': $$Business_fields_updated_at,
  'media_url': $$Business_fields_media_url
}, getters: const [
  'business_id',
  'name',
  'description',
  'active',
  'media_id',
  'created_at',
  'updated_at',
  'media_url'
], setters: const [
  'business_id',
  'name',
  'description',
  'active',
  'media_id',
  'created_at',
  'updated_at',
  'media_url'
]);

_BussinessReply__Constructor([positionalParams, namedParams]) =>
    new BussinessReply();

const BussinessReplyClassMirror = const ClassMirror(
    name: 'BussinessReply',
    constructors: const {
      '': const FunctionMirror(name: '', $call: _BussinessReply__Constructor)
    },
    fields: const {
      'msg': $$Reply_fields_msg,
      'data': $$Reply_fields_data,
      'code': $$Reply_fields_code,
      'ok': $$Reply_fields_ok
    },
    getters: const ['msg', 'data', 'code', 'ok'],
    setters: const ['msg', 'data', 'code', 'ok'],
    superclass: Reply);
